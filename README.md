# GITLAB-CI EXAMPLE: DOTNET CORE

[![pipeline status](https://gitlab.com/tobiaskoch/gitlab-ci-example-dotnetcore/badges/master/pipeline.svg)](https://gitlab.com/e.ametrano/gitlab-ci-example-dotnetcore/commits/master)
[![maintained: yes](https://tobiaskoch.gitlab.io/badges/maintained-yes.svg)](https://gitlab.com/e.ametrano/gitlab-ci-example-dotnetcore/commits/master)
[![coverage report](https://gitlab.com/e.ametrano/gitlab-ci-example-dotnetcore/badges/master/coverage.svg)](https://gitlab.com/e.ametrano/gitlab-ci-example-dotnetcore/badges/master/coverage.svg)

---

This is a simple [gitlab continuous integration](https://about.gitlab.com/features/gitlab-ci-cd/) example project (compatible with the [linux-based shared runners](https://docs.gitlab.com/runner/) provided on [gitlab.com](https://gitlab.com)) using the official debian linux based [dotnet docker image](https://hub.docker.com/r/microsoft/dotnet/) to build a .NET Core project.

Thanks to the [Tango Desktop Project](http://tango.freedesktop.org) for the repository icon.

## Contributing
see [CONTRIBUTING.md](https://gitlab.com/tobiaskoch/gitlab-ci-example-dotnetcore/blob/master/CONTRIBUTING.md)

## Contributors
- [v1](https://gitlab.com/tobiaskoch/gitlab-ci-example-dotnetcore/tree/v1): initial version by [Tobias Koch](https://gitlab.com/tobiaskoch)
- [v2](https://gitlab.com/tobiaskoch/gitlab-ci-example-dotnetcore/tree/v2): unit test summary, code coverage analysis and code climate report by [lastlink](https://gitlab.com/lastlink)

## Donating
Thanks for your interest in this project. You can show your appreciation and support further development by [donating](https://www.tk-software.de/donate).

## License
**gitlab-ci-example-dotnetcore** © 2018-2020  Tobias Koch. Released under the [MIT License](https://gitlab.com/tobiaskoch/gitlab-ci-example-dotnetcore/blob/master/LICENSE.md).